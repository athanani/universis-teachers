import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {Injectable} from '@angular/core';

@Injectable({ providedIn: 'root' })
export class CourseClassResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> | any {
    const query = this._context.model('instructors/me/classes').asQueryable()
      .expand('locales,course($expand=locales),department($expand=locales),period,statistic($select=id,eLearningUrl,studyGuideUrl)');

    route.params['id']
      ? query.where('id').equal(route.params['id'])
      : query.where('course').equal(route.parent?.params['course'])
        .and('year').equal(route.parent?.params['year'])
        .and('period').equal(route.parent?.params['period']);

    return query.getItem().then(res => res || null);
  }
}
