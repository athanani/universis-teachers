import { Component, OnInit, OnDestroy, Input, Output, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { CoursesService } from '../../services/courses.service';
import { ActivatedRoute } from '@angular/router';
import { DiagnosticsService, ErrorService } from '@universis/common';
import { LoadingService } from '@universis/common';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-course-books',
  templateUrl: './course-books.component.html',
  styleUrls: ['./course-books.component.scss']
})
export class CourseBooksComponent implements OnChanges {
  @Output() loadingEvent = new EventEmitter<boolean>();
  @Input() selectedClass: any;
  
  public books: any;

  constructor(private _coursesService: CoursesService) {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.loadingEvent.emit(true);
    if (changes.hasOwnProperty('selectedClass')) {
      const selectedClassId = changes['selectedClass'].currentValue.id;
      if (selectedClassId) {
        this._coursesService.getCourseBooks(selectedClassId).then(books => {
          this.books = books;
          this.loadingEvent.emit(false);
        }).catch(err => {
          this.loadingEvent.emit(false);
          console.log(err);
        })
      }
    }
  }

  public openUrl(url) {
    if (url) {
      window.open(url, '_blank');
    }
  }

}
