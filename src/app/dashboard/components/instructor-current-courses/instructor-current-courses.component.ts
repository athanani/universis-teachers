import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
//import {recognize} from '@angular/router/src/recognize';
import {CoursesService} from '../../../courses/services/courses.service';
import {ErrorService, ConfigurationService} from '@universis/common';

@Component({
  selector: 'app-instructor-current-courses',
  templateUrl: './instructor-current-courses.component.html',
  styleUrls: ['./instructor-current-courses.component.scss']
})
export class InstructorCurrentCoursesComponent implements OnInit {
  public defaultLanguage: string | any = "";
  public currentLanguage: string = "";

  public recentCourses: any = []; // Data
  public isLoading = true;        // Only if data is loaded

  constructor(private _context: AngularDataContext,
              private _coursesService: CoursesService,
              private _errorService: ErrorService,
              private _configurationService: ConfigurationService) { 
                this.currentLanguage = this._configurationService.currentLocale;
                this.defaultLanguage = this._configurationService.settings?.i18n?.defaultLocale;
              }

  ngOnInit() {


    this._coursesService.getRecentCourses().then((res) => {
      this.recentCourses = res;
      this.isLoading = false;
    }).catch(err => {
      return this._errorService.navigateToError(err);
    });
  }

}
