import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@universis/common';
import { ConsultedStudentsHomeComponent } from './components/consulted-students-home/consulted-students-home.component';
import { ConsultedStudentsTableComponent } from './components/consulted-students-table/consulted-students-table.component';
// tslint:disable-next-line:max-line-length
import { ConsultedStudentsTableConfigurationResolver, ConsultedStudentsTableSearchResolver } from './components/consulted-students-table/consulted-students-table-config.resolver';
import { ConsultedStudentsDetailsComponent } from './components/consulted-students-details/consulted-students-details.component';
import { StudentDetailsGeneralComponent } from './components/consulted-students-details/student-details-general/student-details-general.component';
import { StudentDetailsCoursesComponent } from './components/consulted-students-details/student-details-courses/student-details-courses.component';
import { StudentDetailsRegistrationsComponent } from './components/consulted-students-details/student-details-registrations/student-details-registrations.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [
      AuthGuard
    ],
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/active'
      },
      {
        path: "list",
        component: ConsultedStudentsHomeComponent,
        children: [
          {
            path: ':list',
            component: ConsultedStudentsTableComponent,
            resolve: {
              tableConfiguration: ConsultedStudentsTableConfigurationResolver,
              searchConfiguration: ConsultedStudentsTableSearchResolver
            }
          }
        ]
      },
      {
        path: ':studentId',
        component: ConsultedStudentsDetailsComponent,
        children: [
          {
            path: '',
            redirectTo: 'details',
            pathMatch: 'full'
          },
          {
            path: 'details',
            component: StudentDetailsGeneralComponent,
          },
          {
            path: 'courses',
            component: StudentDetailsCoursesComponent,
          },
          {
            path: 'registrations',
            component: StudentDetailsRegistrationsComponent,
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsultedStudentsRoutingModule { }
