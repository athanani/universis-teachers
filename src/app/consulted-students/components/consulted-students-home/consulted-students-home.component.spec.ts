import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ConsultedStudentsHomeComponent } from './consulted-students-home.component';



describe('ConsultedStudentsHomeComponent', () => {
  let component: ConsultedStudentsHomeComponent;
  let fixture: ComponentFixture<ConsultedStudentsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultedStudentsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultedStudentsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
