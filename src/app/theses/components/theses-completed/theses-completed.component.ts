import { Component, HostListener, OnInit } from '@angular/core';
import { ThesesService } from '../../services/theses.service';
import { NavigationEnd, Router } from '@angular/router';
import { LoadingService } from '@universis/common';
import { ErrorService } from '@universis/common';
import { ProfileService } from 'src/app/profile/services/profile.service';

@Component({
  selector: 'app-theses-completed',
  templateUrl: './theses-completed.component.html',
  styleUrls: ['./theses-completed.component.scss']
})

export class ThesesCompletedComponent implements OnInit {
  public completedTheses: any = [];
  public isLoading = true;
  public selectedIndex = 0;
  public isSrolling = true;
  public count: any;
  public searchText = '';
  public selectedThesisType = null;
  public select: any = [];
  public instructor: any;

  constructor(private thesesServices: ThesesService,
    private router: Router,
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private profileService: ProfileService
  ) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const tree = router.parseUrl(router.url);
        if (tree.fragment) {
          const element = document.querySelector('#' + tree.fragment);
          if (element) {
            element.scrollIntoView();
          }
        }
      }
    });
  }

  ngOnInit() {
    this._loadingService.showLoading();

    this.profileService.getInstructor().then(res => {
      this.instructor = res;
    }).catch( err => {
      return this._errorService.navigateToError(err);
    });

    this.thesesServices.getCompletedTheses().then((res1: any) => {
      this.completedTheses = this.updateResultsInCompletedTheses(res1);
      this.count = res1.length;
      this.isLoading = false;
      this._loadingService.hideLoading();
    }).catch(err => {
      // hide loading
      this._loadingService.hideLoading();
      return this._errorService.navigateToError(err);
    });
    this.thesesServices.getCompletedThesesSelectValues()
      .then((res2: any) => { this.select = res2; })
      .catch((err: any) => this._errorService.navigateToError(err));
  }

  search() {
    const query = this.thesesServices.searchCompletedTheses(this.searchText);
    if (this.selectedThesisType) {
      query.and('type/alternateName').equal(this.selectedThesisType);
    }
    try {
      this.isLoading = true;
      this._loadingService.showLoading();
      query.getItems().then((res: any) => {
        this.completedTheses = this.updateResultsInCompletedTheses(res);
        this.count = res.length;
        this.isLoading = false;
        this._loadingService.hideLoading();
      }).catch(err => {
        // hide loading
        this._loadingService.hideLoading();
        return this._errorService.navigateToError(err);
      });
    } catch (err) {
      this._loadingService.hideLoading();
      console.error(err);
    }
  }

  setIndex(index: number) {
    this.selectedIndex = index;
    this.isSrolling = false;
  }

  @HostListener('scroll', ['$event'])
  scrollHandler(event) {
    return this.getScroll();
  }

  public getScroll( node:any = document.getElementById('inner__content') ): number {

    if (!this.isSrolling) {
      this.isSrolling = true;
      return 0;
    }

    const currentScroll = node.scrollTop;
    const elements = document.getElementsByClassName('years');
    let fountActive = false;

    this.selectedIndex = -1;

    for (let i = 0; i < elements.length; i++) {
      const currentElement: any = elements.item(i);

      let currentOffSetTop = currentElement.getBoundingClientRect().top;
      if (currentOffSetTop < 0) {
        currentOffSetTop *= -1;
      }

      if (((currentElement.getBoundingClientRect().top > 0) || (currentElement.getBoundingClientRect().height - 60) >
        (node.getBoundingClientRect().top + currentOffSetTop)) && !fountActive) {

        this.selectedIndex = i;
        fountActive = true;
      }
    }
    return (currentScroll);
  }

  // Update results within completed theses - instructor role
  updateResultsInCompletedTheses(theses: any) {
   theses.forEach(thesis => {
      thesis.students.forEach(student => {
        student.results.forEach(result => {
          const instructor = thesis.members.find(item => item.member.id === result.instructor.id);
            result.instructor.roleName = instructor && instructor.roleName ? instructor.roleName : null;
        });
        student.results.sort((a, b) => {
          return a.index < b.index ? -1 : 1;
        });
      });
    });
    return theses;
  }
  

}
