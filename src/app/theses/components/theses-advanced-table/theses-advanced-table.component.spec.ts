import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ThesesAdvancedTableComponent } from './theses-advanced-table.component';



describe('ThesesAdvancedTableComponent', () => {
  let component: ThesesAdvancedTableComponent;
  let fixture: ComponentFixture<ThesesAdvancedTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThesesAdvancedTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThesesAdvancedTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
