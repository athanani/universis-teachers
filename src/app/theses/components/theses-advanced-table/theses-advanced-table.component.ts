import { Component, ViewChild, Input, OnDestroy, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult } from '@universis/ngx-tables';
import { Subscription } from 'rxjs';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import { ActivatedRoute } from '@angular/router';
import { ActivatedTableService } from '@universis/ngx-tables';
import { AdvancedTableSearchComponent } from '@universis/ngx-tables';
import { ErrorService } from '@universis/common';
import { ProfileService } from 'src/app/profile/services/profile.service';
@Component({
  selector: 'app-theses-advanced-table',
  templateUrl: './theses-advanced-table.component.html',
  styleUrls: ['./theses-advanced-table.component.scss']
})
export class ThesesAdvancedTableComponent implements AfterViewInit, OnDestroy {

  private dataSubscription?: Subscription;
  public selectedItems: any[] = [];
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table!: AdvancedTableComponent;
  @ViewChild('search') search!: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch!: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  public recordsTotal: any;
  private fragmentSubscription?: Subscription;
  public instructor: any;

  constructor (private _activatedRoute: ActivatedRoute,
    private _activatedTable: ActivatedTableService,
    private _errorService: ErrorService,
    private profileService: ProfileService) { }

    ngAfterViewInit() {

      this.profileService.getInstructor().then(res => {
        this.instructor = res;
      }).catch( err => {
        return this._errorService.navigateToError(err);
      });

      this.dataSubscription = this._activatedRoute.data.subscribe((data: { searchConfiguration: any, tableConfiguration: any }) => {
      try {
          this._activatedTable.activeTable = this.table;
          this.searchConfiguration = data.searchConfiguration;
  
          if (data['tableConfiguration']) {
            // set config
            this.table.config = AdvancedTableConfiguration.cast(data['tableConfiguration']);
            // reset search text
            this.advancedSearch.text = "";
  
            // reset table
            this.table.reset(true);
          }
        } catch (err) {
          this._errorService.navigateToError(err);
        }
      });
    }
  
  ngOnDestroy() {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }
  
  onDataLoad(event: AdvancedTableDataResult) {
    this.recordsTotal = event.recordsTotal;
  }

}
