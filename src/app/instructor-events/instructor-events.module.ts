import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { EventsModule } from '@universis/ngx-events';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { SharedModule } from '@universis/common';
import { MostModule } from '@themost/angular';
import { InstructorEventComponent } from './components/instructor-event/instructor-event.component';
import { InstructorEventRoutingModule } from './instructor-event-routing.module';

import * as el from "./i18n/instructor-events.el.json"
import * as en from "./i18n/instructor-events.en.json"

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    MostModule,
    InstructorEventRoutingModule,
    EventsModule
  ],
  declarations: [
    InstructorEventComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InstructorEventsModule {
  constructor(private _translateService: TranslateService) {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }
}
