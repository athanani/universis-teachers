import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ErrorService } from '@universis/common';
import { LoadingService } from '@universis/common';
import { Subscription } from 'rxjs';
import { ResponseError } from '@themost/client';

@Component({ template: '' })
export abstract class BasicLoadComponent implements OnInit, OnDestroy {

  public isLoading = true;
  protected routeParams: any;

  private fragmentSubscription: Subscription;
  private paramsSubscription: Subscription;

  constructor(protected errorService: ErrorService,
    protected context: AngularDataContext,
    protected loadingService: LoadingService,
    protected route: ActivatedRoute) {
  }

  abstract load(routeParams: Params): Promise<any>;

  ngOnInit() {

    this.paramsSubscription = this.route.parent.params.subscribe(async routeParams => {
      this.routeParams = routeParams;
      this.loadData();
    });

    this.fragmentSubscription = this.route.fragment.subscribe(async fragment => {
      if (fragment && fragment === 'reload') {
        this.loadData();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

  async loadData() {
    this.loadingService.showLoading();
    try {
      await this.load(this.routeParams)

      this.loadingService.hideLoading();
      this.isLoading = false;
    } catch (err) {
      this.loadingService.hideLoading();
      return this.errorService.navigateToError(err);
    }
  }

}
